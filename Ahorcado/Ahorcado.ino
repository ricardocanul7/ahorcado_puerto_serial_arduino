String palabra[5] = {"h", "o", "l", "a"};
bool validacion[] = {false, false, false, false};

int ledVida[3] = {13, 12, 11};
int ledPalabra[4] = {10, 9, 8, 7};

int contadorVidas = 3;
bool ganaste = false;

void setup() {
  Serial.begin(9600);
  //SALIDAS VIDAS
  pinMode(ledVida[0], OUTPUT);
  pinMode(ledVida[1], OUTPUT);
  pinMode(ledVida[2], OUTPUT);

  //SALIDAS PALABA ACERTADA
  pinMode(ledPalabra[0], OUTPUT);
  pinMode(ledPalabra[1], OUTPUT);
  pinMode(ledPalabra[2], OUTPUT);
  pinMode(ledPalabra[3], OUTPUT);

  inicializarJuego();
}

void inicializarJuego(){
  // Inicializa palabras leds apagados
  digitalWrite(ledPalabra[0], LOW);
  digitalWrite(ledPalabra[1], LOW);
  digitalWrite(ledPalabra[2], LOW);
  digitalWrite(ledPalabra[3], LOW);

  //Inicializa vidas leds encendidos
  digitalWrite(ledVida[0], HIGH);
  digitalWrite(ledVida[1], HIGH);
  digitalWrite(ledVida[2], HIGH);

  validacion[0] = false;
  validacion[1] = false;
  validacion[2] = false;
  validacion[3] = false;
}

void loop() {
  if (Serial.available()) {
    String option = Serial.readStringUntil('\n');

    if (option == palabra[0] || option == palabra[1] || option == palabra[2] || option == palabra[3]) {
      if (option == palabra[0]) {
        Serial.print("h es correcto\n");
        digitalWrite(ledPalabra[0], HIGH);
        validacion[0] = true;
      }
      if (option == palabra[1]) {
        Serial.print("o es correcto\n");
        digitalWrite(ledPalabra[1], HIGH);
        validacion[1] = true;
      }
      if (option == palabra[2]) {
        Serial.print("l es correcto\n");
        digitalWrite(ledPalabra[2], HIGH);
        validacion[2] = true;
      }
      if (option == palabra[3]) {
        Serial.print("a es correcto\n");
        digitalWrite(ledPalabra[3], HIGH);
        validacion[3] = true;
      }
    }else{
      if (contadorVidas > 0) {
          digitalWrite(ledVida[contadorVidas - 1], LOW);
          contadorVidas--;
          if(contadorVidas == 0){
            Serial.print("Has perdido!\n");
            delay(2000);
            inicializarJuego();
          }
        } else {
          Serial.print("Has perdido!\n");
          delay(2000);
          inicializarJuego();
        }
    }

    //Validacion
    int i = 0;
    for (i = 0; i < 4; i++) {
      if (validacion[i] == true) {
        ganaste = true;
      } else {
        ganaste = false;
        break;
      }
    }

    if (ganaste == true) {
      Serial.print("Has ganado!\n");
      delay(2000);
      inicializarJuego();
    }

  }
}
